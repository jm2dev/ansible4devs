#+STARTUP: showall
#+REVEAL_ROOT: http://cdn.jsdelivr.net/reveal.js/3.0.0/
#+OPTIONS: num:nil toc:nil
#+REVEAL_TRANS: Zoom
#+REVEAL_THEME: solarized
#+TITLE: Ansible para programadores
#+AUTHOR: José Miguel
#+EMAIL: jm@0pt1mates.com

* Introducción

- Quién soy
- Qué hago

* Qué quiero

#+BEGIN_QUOTE
Make everything as simpler as possible,
but not simpler.

Albert Einstein
#+END_QUOTE

* Motivación

- Tareas repetitivas
- Procastrinación
- Pedagogía

* Una definición

#+BEGIN_QUOTE
Automated provisioning, also called self-service provisioning, is the
ability to deploy an information technology or telecommunications
service by using predefined procedures that are carried out
electronically without requiring human intervention.
#+END_QUOTE

* Opciones

- **ansible**
- cfengine
- chef
- puppet
- salt

* Objectivo
:PROPERTIES:
:reveal_background: imagenes/SurfaceLaptop.png
:reveal_background_size: 1400px
:reveal_background_trans: slide
:END:

#+BEGIN_EXAMPLE
configúrate
#+END_EXAMPLE

* Ansible

- requisitos básicos
  - ssh
  - python
- lenguaje sencillo
  - yaml

* Preparación

- python 3 con virtualenv

#+BEGIN_EXAMPLE
mkdir proyecto
cd proyecto
python3 -m venv venv
#+END_EXAMPLE

* cat requirements.txt

#+BEGIN_EXAMPLE
ansible
#+END_EXAMPLE

* Inventario: cat hosts

#+CAPTION: Inventory file: hosts
#+BEGIN_EXAMPLE
[local]
127.0.0.1
#+END_EXAMPLE

* Tareas de un programador

Acciones a realizar:
- instalar paquetes
- copiar ficheros
- etc

Nuestros requisitos para un proyecto con scala:
- git
- openjdk
- sbt

* Rol

- Criterio para agrupar tareas.

#+BEGIN_EXAMPLE
mkdir -p roles/programador/tasks
touch roles/programador/tasks/main.yml
#+END_EXAMPLE

* cat programador/tasks/main.yml

#+CAPTION: Main task file: programador/tasks/main.yml
#+BEGIN_EXAMPLE
---
- name: Update pacman cache
  become: true
  pacman:
    update_cache: yes

- name: Install packages
  become: true
  pacman:
    name: "{{ item }}"
    state: present
  with_items:
    - git
    - meld
    - jdk8-openjdk
    - openjdk8-doc
    - openjdk8-src
    - sbt
#+END_EXAMPLE

* cat programador.yml

#+CAPTION: Playbook file: programador.yml
#+BEGIN_EXAMPLE
---
- name: Preparacion proyecto X
  hosts: local
  connection: local
  roles:
    - programador
#+END_EXAMPLE

* Funciona?

#+BEGIN_EXAMPLE
cd proyecto
source venv/bin/activate
pip install -r requirements.txt
ansible-playbook -i hosts programador.yml
#+END_EXAMPLE

* Rol por proyecto

Llegados a este punto, tenemos una buena base para empezar a trabajar.

Hay más candidatos para automatizar?

* Tareas versión 2

Aprovechamos la oportunidad para limpiar un poco nuestro fichero principal de tareas.

#+BEGIN_EXAMPLE
mv roles/programador/tasks/main.yml \
   roles/programador/tasks/paquetes.yml
#+END_EXAMPLE

* cat roles/programador/tasks/sbt.yml

#+BEGIN_EXAMPLE
---
- name: SBT config plugins directory
  file:
    path: "{{ home_dir }}/.sbt/1.0/plugins"
    recurse: yes
    state: directory
    mode: 0755

- name: SBT Global plugins
  copy:
    src: files/plugins.sbt
    dest: "{{ home_dir }}/.sbt/1.0/plugins/plugins.sbt"

- name: SBT Proxy repositories
  copy:
    src: files/repositories.sbt
    dest: "{{ home_dir }}/.sbt/repositories"
#+END_EXAMPLE

* Esas variables 

#+BEGIN_EXAMPLE
touch group_vars/all
#+END_EXAMPLE

#+BEGIN_EXAMPLE
---
home_dir: "{{ lookup('env', 'HOME') }}"
#+END_EXAMPLE

* Configurando SBT (II)

#+BEGIN_EXAMPLE
mkdir -p roles/programador/files
touch roles/programador/files/plugins.sbt
touch roles/programador/files/repositories.sbt
#+END_EXAMPLE

* cat roles/programador/files/plugins.sbt
#+BEGIN_EXAMPLE
addSbtPlugin("org.ensime" % "sbt-ensime" % "2.1.0")

addSbtPlugin("io.get-coursier" % "sbt-coursier" % "1.1.0-M1")

addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.9.0")

addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.5.1")
#+END_EXAMPLE

* cat roles/programador/files/repositories.sbt
#+BEGIN_EXAMPLE
[repositories]
  local
  my-ivy-proxy-releases: https://nexus.proyecto.xyz/content/repositories/ivy-releases/, \
    [organization]/[module]/(scala_[scalaVersion]/)(sbt_[sbtVersion]/)[revision]/[type]s/[artifact](-[classifier]).[ext]
  my-maven-proxy-releases: https://nexus.proyecto.xyz/content/repositories/central/
#+END_EXAMPLE

* cat roles/programador/tasks/git.yml

#+BEGIN_EXAMPLE
---
- name: Git config directory
  file:
    path: "{{ home_dir }}/.config/git"
    state: directory
    mode: 0755

- name: Git setup
  template:
    src: templates/git_config.j2
    dest: "{{ home_dir }}/.config/git/config"
#+END_EXAMPLE

* Configurando GIT (II)

Qué son las plantillas?

#+BEGIN_EXAMPLE
mkdir -p roles/programador/templates
touch roles/programador/templates/git_config.j2
#+END_EXAMPLE

* cat roles/programador/templates/git_config.j2

#+BEGIN_EXAMPLE
[user]
	name = {{ git_username }}
	email = {{ git_email }}

[color]
	ui = auto

[push]
	default = simple

[core]
	editor = emacsclient -nw

[merge]
  tool = meld
#+END_EXAMPLE

* Configurando GIT (IV)

Y eso son ... variables?

#+BEGIN_EXAMPLE
mkdir -p roles/programador/vars
touch roles/programador/vars/main.yml
#+END_EXAMPLE

* cat roles/programador/vars/main.yml

#+BEGIN_EXAMPLE
---
git_username: Jose Miguel Martinez Carrasco
git_email: jm@0pt1mates.com
#+END_EXAMPLE

* Tareas de programador (II)

Y la nueva versión de **main.yml** queda:

#+BEGIN_EXAMPLE
---
- import_tasks: paquetes.yml
- import_tasks: git.yml
- import_tasks: sbt.yml
#+END_EXAMPLE

* Rol proyecto X

Llegados a este punto tenemos un entorno de trabajo básico pero funcional.

A continuación descubrimos algunos requisitos adicionales del proyecto, como una base de datos.

Ya hemos aprendido lo básico, así que sin muchas sorpresas vamos a
crear unas nuevas tareas para tener una base de datos contra la que
desarrollar.

* Tareas

#+BEGIN_EXAMPLE
mkdir -p roles/proyectoX/tasks
touch roles/proyectoX/tasks/main.yml
touch roles/proyectoX/tasks/database.yml
#+END_EXAMPLE

#+BEGIN_EXAMPLE
---
- import_tasks: database.yml
#+END_EXAMPLE

* Base de datos

Aunque podría ser cualquier otra cosa.

#+CAPTION: Main task file: proyectoX/tasks/database.yml
#+BEGIN_EXAMPLE
---
- name: Update pacman cache
  become: true
  pacman:
    update_cache: yes

- name: Install postgresql
  become: true
  pacman:
    name: "{{ item }}"
    state: present
  with_items:
    - postgresql
    - postgresql-docs

- name: Start postgresql service
  systemd:
    name: postgresql
    state: started
#+END_EXAMPLE

* Fatjar

Aquí quiero mostrar cómo generamos un fatjar del proyecto, y lo ejecutamos localmente como servicio, para:

- ejecutar tests de integración
- tests funcionales

* Systemd unit

#+BEGIN_EXAMPLE
mkdir -p roles/fatjar/{tasks,templates,vars}
touch roles/fatjar/tasks/main.yml
touch roles/fatjar/templates/fatjar.service.j2
touch roles/fatjar/vars/all
#+END_EXAMPLE

* cat roles/fatjar/vars/all

#+BEGIN_EXAMPLE
service_user: "hello"
service_name: "Hello"
service_install_dir: "/opt/{{ service_name }}"
bin_location: "{{ service_install_dir }}/bin/server"
#+END_EXAMPLE

* cat roles/fatjar/templates/fatjar.service.j2

#+BEGIN_EXAMPLE
[Unit]
Description={{ service_name }}
After=syslog.target

[Service]
ExecStart={{ bin_location }}
User={{ service_user }}

[Install]
WantedBy=multi-user.target
#+END_EXAMPLE

* cat roles/fatjar/tasks/main.yml

#+BEGIN_EXAMPLE
- name: Create systemd directory
  file:
    path: "{{ service_install }}"
    state: directory
    mode: 0755

- name: Install fatjar service
  copy:
    src: "item"
    dest: "{{ service_install }}"
  with_items:
    - "{{ proyectox }}/target/pack/bin"
    - "{{ proyectox }}/target/pack/lib"

- name: Generate systemd unit
  template:
    src: fatjar.service.j2
    dest: "/lib/systemd/system/{{ service_name }}.service"
    mode: "u=rwx,g=rx,o=rx"

- name: Start service
  systemd: 
    state: started
    name: {{ service_name }}
#+END_EXAMPLE

* Playbook

#+BEGIN_EXAMPLE
---
- name: Preparacion proyecto X
  hosts: local
  connection: local
  roles:
    - programador
    - proyectoX
    - fatjar
#+END_EXAMPLE

* Conclusión

#+BEGIN_EXAMPLE
curl localhost:8080/hello/opensouthcode
#+END_EXAMPLE

#+BEGIN_EXAMPLE
{                                                                                                                                    
  "message": "Hello, Open South Code"                                                                                                   
}
#+END_EXAMPLE

* Contacto

@jm2dev

