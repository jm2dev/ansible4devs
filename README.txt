Table of Contents
─────────────────

1 Introducción
2 Notas


1 Introducción
══════════════

  Mi presentación para el [OpenSouthCode 2018] en Málaga.


[OpenSouthCode 2018]
https://www.opensouthcode.org/conferences/opensouthcode2018


2 Notas
═══════

  Es una presentación realizada con [org-mode] y [revealjs], para
  imprimir el pdf se debe añadir el parámetro *print-pdf* para que se
  impriman todas las páginas.

  ┌────
  │ firefox ansible4devs-sol.html?print-pdf
  └────


[org-mode] https://orgmode.org/

[revealjs] https://revealjs.com
